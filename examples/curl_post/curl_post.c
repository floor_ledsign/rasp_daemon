#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

int main() {

    char *url_post = "https://google.com/";
    char *url_get = "https://google.com/";

    CURL *curl;
    CURLcode res;
    curl = curl_easy_init();

    struct curl_slist *list = NULL;

    curl = curl_easy_init();

    // =========================================
    // METHOD : POST
    if(curl) {

        list = NULL;

        // URL
        curl_easy_setopt(curl, CURLOPT_URL, url_post);

        // METHOD
        curl_easy_setopt(curl, CURLOPT_POST, 1L);

        // HEADERS
        list = curl_slist_append(list, "Content-Type: application/json");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);

        // SSL
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 1L);

        // DATA
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "data");

        // EXECUTE
        res = curl_easy_perform(curl);

        curl_easy_cleanup(curl);

        // callback 함수가 없으면 stdout 으로 출력된다.
    }
}
