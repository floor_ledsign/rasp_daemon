#include <stdio.h>
#include <curl/curl.h>
#include <string>

class CFtpFile
{
public:
 CFtpFile() : m_psttFd(NULL)
 {
 }
 
 ~CFtpFile()
 {
  if( m_psttFd )
  {
   fclose( m_psttFd );
   m_psttFd = NULL;
  }
 }
 
  std::string m_strFileName;
  FILE * m_psttFd;
};

// 파일 수신시 호출되는 함수
static size_t FtpDownload( void * pszBuf, size_t iSize, size_t iCount, void *stream )
{
  CFtpFile * pclsFile = (CFtpFile *)stream;
  if( pclsFile && !pclsFile->m_psttFd )
  {
    pclsFile->m_psttFd = fopen( pclsFile->m_strFileName.c_str(), "wb" );
    if( !pclsFile->m_psttFd )
      return -1;
  }
 
  return fwrite( pszBuf, iSize, iCount, pclsFile->m_psttFd );
}

// 아래와 같이 실행한다.
//
// ./a.out ftp://localhost/download_file save_file
// ./a.out ftp://id:pw@localhost/download_file save_file
//
int main( int argc, char * argv[] )
{
  CURL *curl;
  CURLcode res;
  CFtpFile clsFile;
 
  if( argc != 3 )
  {
   printf( "[Usage] %s {ftp url} {save filename}\n", argv[0] );
   return 0;
  }
 
  std::string strUrl = argv[1];
  clsFile.m_strFileName = argv[2];

  curl_global_init(CURL_GLOBAL_DEFAULT);

  curl = curl_easy_init();
  if( curl )
  {
    curl_easy_setopt( curl, CURLOPT_URL, strUrl.c_str() );
    curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, FtpDownload );
    curl_easy_setopt( curl, CURLOPT_WRITEDATA, &clsFile );

    // 실행 과정을 출력하고 싶으면 아래의 주석을 제거한다.
    //curl_easy_setopt( curl, CURLOPT_VERBOSE, 1L );
    res = curl_easy_perform( curl );

    curl_easy_cleanup( curl );

    if( CURLE_OK != res )
    {
      fprintf( stderr, "curl told us %d\n", res );
    }
  }

  curl_global_cleanup();

  return 0;
}
