#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

struct MemoryStruct {
    char *memory;
    size_t size;
};

size_t write_to_memory_callback(void *buffer, size_t size, size_t nmemb, void *userp) {

    size_t realsize = size * nmemb;
    struct MemoryStruct *mem = (struct MemoryStruct *) userp;
    char *ptr = (char *) realloc(mem->memory, mem->size + realsize + 1);

    if(!ptr) {
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }

    mem->memory = ptr;
    memcpy(&(mem->memory[mem->size]), buffer, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

int main() {

    char *url_post = "https://google.com/";
    char *url_get = "https://google.com/";

    CURL *curl;
    CURLcode res;
    curl = curl_easy_init();

    struct curl_slist *list = NULL;

    // =========================================
    // METHOD : POST & CALLBACK
    if(curl) {

        list = NULL;

        struct MemoryStruct chunk;
        chunk.memory = (char *) malloc(1);
        chunk.size = 0;

        // URL
        curl_easy_setopt(curl, CURLOPT_URL, url_post);

        // METHOD
        curl_easy_setopt(curl, CURLOPT_POST, 1L);

        // HEADERS
        list = curl_slist_append(list, "Content-Type: application/json");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);

        // SSL
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 1L);

        // DATA
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "data");

        // CALLBACK
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_memory_callback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &chunk);

        // EXECUTE
        res = curl_easy_perform(curl);

        curl_easy_cleanup(curl);

        if (chunk.size > 0) {
            printf("111111111 : %s", chunk.memory);
        }
        free(chunk.memory);
    }
}