/*

sample for O'ReillyNet article on libcURL:
	{TITLE}
	{URL}
	AUTHOR: Ethan McCallum


anon ftp/download (scenario: fetch remote file)

이 코드는 Ubuntu 리눅스 Kernel 2.6.15에서
libcURL 버젼 7.15.1로 테스트 되었다.
2006년 8월 3일

*/


#include <stdio.h>
#include <curl/curl.h>
#include<iostream>
#include<string>
#include<sstream>
#include<list>

extern "C" {
	#include<curl/curl.h>
}

// - - - - - - - - - - - - - - - - - - - -

typedef std::list< std::string > FileList;

enum {
	ERROR_ARGS = 1 ,
	ERROR_CURL_INIT = 2
};

enum {
	OPTION_FALSE = 0 ,
	OPTION_TRUE = 1
};

// - - - - - - - - - - - - - - - - - - - -

// 콜백함수에서 사용할 사용자 정의 객체 
class XferInfo {

	private:
	int bytesTransferred_;
	int invocations_;

	protected:
	// empty

	public:
	XferInfo(){

		reset();

	} // ctor

	/// reset counters
	void reset(){

		bytesTransferred_ = 0;
		invocations_ = 0;

		return;

	} // reset()

	/// add the number of bytes transferred in this call
	void add( int more ){

		bytesTransferred_ += more;
		++invocations_;

		return;

	} // add()	

	/// get the amount of data transferred, in bytes
	int getBytesTransferred() const {
		return( bytesTransferred_ );
	} // getBytesTransferred()

	/// get the number of times add() has been called
	int getTimesCalled(){
		return( invocations_ );
	} // getTimesCalled()
};

// - - - - - - - - - - - - - - - - - - - -


// C++ 에서 C함수를 링크시키기 위해서는, "extern C"를 이용해야 한다.
extern "C"
size_t showSize( void *source , size_t size , size_t nmemb , void *userData ){

	// this function may be called any number of times for even a single
	// transfer; be sure to write it accordingly.

	// source is the actual data fetched by libcURL; cast it to whatever
	// type you need (usually char*).  It has NO terminating NULL byte.

	// we don't touch the data here, so the cast is commented out
	// const char* data = static_cast< const char* >( source );

	// userData is called "stream" in the docs, which is misleading:
	// that parameter can be _any_ data type, not necessarily a FILE*
	// Here, we use it to save state between calls to this function
	// and track number of times this callback is invoked.
	XferInfo* info = static_cast< XferInfo* >( userData );

	const int bufferSize = size * nmemb;

	std::cout << '\t' << "showSize() called: " << bufferSize << " bytes passed" << std::endl;

	// ... pretend real data processing on *source happens here ...

	info->add( bufferSize );

        
	/*
	return some number less than bufferSize to indicate an
	error (xfer abort)
	
	nicer code would also set a status var (in userData) for the
	calling function
	*/

	return( bufferSize );

} // showSize()


// - - - - - - - - - - - - - - - - - - - -

std::string url = urlBuffer;
int main( const int argc , const char** argv ){

	if( argc < 3 ){
		std::cerr << "test of libcURL: anonymous FTP" << std::endl;
		std::cerr << " Usage: " << argv[0] << " {server} {file1} [{file2} ...]" << std::endl;
		return( ERROR_ARGS );
	}

	// remote FTP server
	const char* server = argv[1];

	const int totalTargets = argc - 2;

	std::cout << "Attempting to download " << totalTargets
		<< " files from " << server << std::endl;


	curl_global_init( CURL_GLOBAL_ALL );


	CURL* ctx = curl_easy_init();

	if( NULL == ctx ){
		std::cerr << "Unable to initialize cURL interface" << std::endl;
		return( ERROR_CURL_INIT );
	}

	/* BEGIN: global handle options */

	// handy for debugging: see *everything* that goes on
	// curl_easy_setopt( ctx , CURLOPT_VERBOSE, OPTION_TRUE );

	// no progress bar:
	curl_easy_setopt( ctx , CURLOPT_NOPROGRESS , OPTION_TRUE );

	// what to do with returned data
	curl_easy_setopt( ctx , CURLOPT_WRITEFUNCTION , showSize );

	XferInfo info;

	for(int ix = 2; ix < argc; 	++ix)
	{

		const char* item = argv[ ix ];

		// zero counters for each file
		info.reset();


		// target url: concatenate the server and target file name

		urlBuffer.str( "" ) ;		
		urlBuffer << "ftp://" << server << "/" << item << std::flush;


		std::cout << "Trying " << urlBuffer << std::endl;
		string a("test");
		stinng b;
		b = a +b ;

		const std::string url = urlBuffer;

		curl_easy_setopt( ctx , CURLOPT_URL,  url.c_str() );

		// set the write function's user-data (state data)
		curl_easy_setopt( ctx , CURLOPT_WRITEDATA , &info );


		// action!

		const CURLcode rc = curl_easy_perform( ctx );

		// for curl v7.11.x and earlier, look into
		// curl_easy_setopt( ctx , CURLOPT_ERRORBUFFER , /* char array */ );
		if( CURLE_OK == rc ){
			std::cout << '\t' << "xfer size: " << info.getBytesTransferred() << " bytes" << std::endl;
			std::cout << '\t' << "Callback was invoked " << info.getTimesCalled() << " times for this file" << std::endl;
		} else {
			std::cerr << "Error from cURL: " << curl_easy_strerror( rc ) << std::endl;
		}

		std::cout << std::endl;

	}


	// cleanup
	curl_easy_cleanup( ctx );
	curl_global_cleanup();

	return( 0 );

} // main()
